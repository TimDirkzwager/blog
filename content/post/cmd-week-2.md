--- 
title: week 2
subtitle: maandag
date: 2017-9-4
---

Vandaag was de eerste studiodag. We hebben eerst uitleg gekregen over wat er van ons verwacht wordt tijdens de studio uren en daarna zijn we gelijk aan de slag gegaan.
We kwamen al snel met een redelijk idee en begonnen het uit te werken, maar na overleg met de docent kwamen we erachter dat we een aantal stappen vergeten waren en die eerst uit moesten werken.
Op het rooster stond dat we een workshop zouden hebben, maar die ging niet door. We hebben deze tijd gebruikt om verder te gaan met ons project.

---
subtitle: dinsdag
date: 2017-9-5
---

Vandaag heb ik het eerste hoorcollege design theory gehad.
Hierin werd het begrip design uitgelegd, waardoor mij duidelijk werd dat breder en groter was dan ik oorspronkelijk dacht.

---
subtitle: woensdag
date: 2017-9-6
---

Vandaag was de tweede studiodag. We zijn verder gegaan bij waar we maandag gebleven waren. 
Ik had toendertijd nog geen laptop, dus in plaats van mijn thema en doelgroep uit te werken heb ik samen met de andere "laptop-lozen" gebrainstormt over het project.
Deze keer was er wel een workshop, over bloggen. Hier hebben we geleerd over markdown en waarom het nodig is je blog bij te houden.

---
subtitle: donderdag
date: 2017-9-7
---

Het eeste werkcollege design theory was vandaag. We moesten hier een visualisatie maken van het proces van hoe wij een ongunstige situatie hebben omgezet naar een gunstige situatie.
Wanneer iedereen dit af had moest je elkaar feedback geven over elkaars visualisaties. 
Omdat ik zelf niet snel een situatie heb kunnen bedenken waarin ik dat gedaan heb, heb ik zelf mijn visualisatie niet afgekregen.
Ik heb wel geleerd van de feedback op de visualisaties van anderen.